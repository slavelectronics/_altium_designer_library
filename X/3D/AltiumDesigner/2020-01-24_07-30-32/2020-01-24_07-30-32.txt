# Created by Ultra Librarian Gold 8.3.217 Copyright � 1999-2018
# Frank Frank, Accelerated Designs

StartFootprints

Footprint (Name "CON24_1X24_DRB_X")
Pad (Name "1") (Location 226.378, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "2") (Location 206.693, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "3") (Location 187.0079, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "4") (Location 167.3229, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "5") (Location 147.6378, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "6") (Location 127.9528, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "7") (Location 108.2678, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "8") (Location 88.5827, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "9") (Location 68.8977, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "10") (Location 49.2127, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "11") (Location 29.5276, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "12") (Location 9.8426, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "13") (Location -9.8425, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "14") (Location -29.5275, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "15") (Location -49.2125, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "16") (Location -68.8976, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "17") (Location -88.5826, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "18") (Location -108.2677, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "19") (Location -127.9527, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "20") (Location -147.6377, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "21") (Location -167.3228, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "22") (Location -187.0078, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "23") (Location -206.6929, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "24") (Location -226.3779, 96.5) (Surface True) (Rotation 180) (ExpandMask 0) (ExpandPaste 0)
PadShape (Size 10, 32) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "25") (Location -291.5, 254.5) (Surface True) (Rotation 0)
PadShape (Size 60, 67) (Shape Rectangular) (Layer Top)
EndPad
Pad (Name "26") (Location 291.5, 254.5) (Surface True) (Rotation 0)
PadShape (Size 60, 67) (Shape Rectangular) (Layer Top)
EndPad
Line (Width 6) (Start -331.5, 70.5) (End 331.5, 70.5) (Layer Mechanical15) 
Line (Width 6) (Start 331.5, 70.5) (End 331.5, 299.5) (Layer Mechanical15) 
Line (Width 6) (Start 331.5, 299.5) (End -331.5, 299.5) (Layer Mechanical15) 
Line (Width 6) (Start -331.5, 299.5) (End -331.5, 70.5) (Layer Mechanical15) 
Polygon (PointCount 4) (Layer Mechanical15) 
Point (-331.5, 70.5)
Point (331.5, 70.5)
Point (331.5, 299.5)
Point (-331.5, 299.5)
EndPolygon
Polygon (PointCount 4) (Layer Mechanical5) 
Point (320, 304.5)
Point (320, 65.5)
Point (-320, 65.5)
Point (-320, 304.5)
EndPolygon
Line (Width 1) (Start 0, 96.5) (End 420, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start 305, 96.5) (End 420, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 405, 146.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 405, 46.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 400, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 410, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start 400, 106.5) (End 410, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 400, 86.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 410, 86.5) (Layer Mechanical7) 
Line (Width 1) (Start 400, 86.5) (End 410, 86.5) (Layer Mechanical7) 
Line (Width 1) (Start 291.5, 254.5) (End 420, 254.5) (Layer Mechanical7) 
Line (Width 1) (Start 0, 96.5) (End 420, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 254.5) (End 405, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 254.5) (End 400, 244.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 254.5) (End 410, 244.5) (Layer Mechanical7) 
Line (Width 1) (Start 400, 244.5) (End 410, 244.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 400, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start 405, 96.5) (End 410, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start 400, 106.5) (End 410, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 289.5) (End -420, 289.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 96.5) (End -420, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start -405, 289.5) (End -405, 96.5) (Layer Mechanical7) 
Line (Width 1) (Start -405, 289.5) (End -410, 279.5) (Layer Mechanical7) 
Line (Width 1) (Start -405, 289.5) (End -400, 279.5) (Layer Mechanical7) 
Line (Width 1) (Start -410, 279.5) (End -400, 279.5) (Layer Mechanical7) 
Line (Width 1) (Start -405, 96.5) (End -410, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start -405, 96.5) (End -400, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start -410, 106.5) (End -400, 106.5) (Layer Mechanical7) 
Line (Width 1) (Start -291.5, 254.5) (End -291.5, 404.5) (Layer Mechanical7) 
Line (Width 1) (Start 291.5, 254.5) (End 291.5, 404.5) (Layer Mechanical7) 
Line (Width 1) (Start -291.5, 389.5) (End 291.5, 389.5) (Layer Mechanical7) 
Line (Width 1) (Start -291.5, 389.5) (End -281.5, 394.5) (Layer Mechanical7) 
Line (Width 1) (Start -291.5, 389.5) (End -281.5, 384.5) (Layer Mechanical7) 
Line (Width 1) (Start -281.5, 394.5) (End -281.5, 384.5) (Layer Mechanical7) 
Line (Width 1) (Start 291.5, 389.5) (End 281.5, 394.5) (Layer Mechanical7) 
Line (Width 1) (Start 291.5, 389.5) (End 281.5, 384.5) (Layer Mechanical7) 
Line (Width 1) (Start 281.5, 394.5) (End 281.5, 384.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 289.5) (End -305, 504.5) (Layer Mechanical7) 
Line (Width 1) (Start 305, 289.5) (End 305, 504.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 489.5) (End 305, 489.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 489.5) (End -295, 494.5) (Layer Mechanical7) 
Line (Width 1) (Start -305, 489.5) (End -295, 484.5) (Layer Mechanical7) 
Line (Width 1) (Start -295, 494.5) (End -295, 484.5) (Layer Mechanical7) 
Line (Width 1) (Start 305, 489.5) (End 295, 494.5) (Layer Mechanical7) 
Line (Width 1) (Start 305, 489.5) (End 295, 484.5) (Layer Mechanical7) 
Line (Width 1) (Start 295, 494.5) (End 295, 484.5) (Layer Mechanical7) 
Text (Location -350, -53.5) (Height 50) (Width 3) (Rotation 0) (Layer Mechanical7) (Value "Default Padstyle: RX10Y32D0T")
Text (Location -362.5, -103.5) (Height 50) (Width 3) (Rotation 0) (Layer Mechanical7) (Value "Mounting Padstyle: RX60Y67D0T")
Text (Location -337.5, -153.5) (Height 50) (Width 3) (Rotation 0) (Layer Mechanical7) (Value "Alt 1 Padstyle: OX60Y90D30P")
Text (Location -337.5, -203.5) (Height 50) (Width 3) (Rotation 0) (Layer Mechanical7) (Value "Alt 2 Padstyle: OX90Y60D30P")
Text (Location 425, 84) (Height 25) (Width 1) (Rotation 0) (Layer Mechanical7) (Value "0in/0mm")
Text (Location 425, 163) (Height 25) (Width 1) (Rotation 0) (Layer Mechanical7) (Value "0.158in/4.013mm")
Text (Location -612.5, 180.5) (Height 25) (Width 1) (Rotation 0) (Layer Mechanical7) (Value "0.193in/4.902mm")
Text (Location -100, 409.5) (Height 25) (Width 1) (Rotation 0) (Layer Mechanical7) (Value "0.583in/14.808mm")
Text (Location -93.75, 509.5) (Height 25) (Width 1) (Rotation 0) (Layer Mechanical7) (Value "0.61in/15.494mm")
Line (Width 6) (Start -310, 91.5) (End -244.47790038, 91.5) (Layer TopOverlay) 
Line (Width 6) (Start 310, 91.5) (End 310, 207.89999962) (Layer TopOverlay) 
Line (Width 6) (Start 250.12634579, 294.5) (End -250.12634579, 294.5) (Layer TopOverlay) 
Line (Width 6) (Start -310, 207.89999962) (End -310, 91.5) (Layer TopOverlay) 
Line (Width 6) (Start 244.47800038, 91.5) (End 310, 91.5) (Layer TopOverlay) 
Line (Width 1) (Start 251.37800598, 96.5) (End 226.37800598, 46.5) (Layer Mechanical13) 
Line (Width 1) (Start 201.37800598, 96.5) (End 226.37800598, 46.5) (Layer Mechanical13) 
Line (Width 1) (Start -305, 96.5) (End 305, 96.5) (Layer Mechanical13) 
Line (Width 1) (Start 305, 96.5) (End 305, 289.5) (Layer Mechanical13) 
Line (Width 1) (Start 305, 289.5) (End -305, 289.5) (Layer Mechanical13) 
Line (Width 1) (Start -305, 289.5) (End -305, 96.5) (Layer Mechanical13) 
Step (Name CON24_1X24_DRB_X.stp)
EndFootprint
EndFootprints

StartComponents

Component (Name "XF2M-2415-1A") (PartCount 2) (DesPrefix "J?")
Footprint (Name "CON24_1X24_DRB_X")
EndComponent
EndComponents
